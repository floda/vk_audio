import argparse
import json
import os
import requests
from selenium import webdriver


def get_audio(config, uri):
    try:
        driver = webdriver.Firefox()
    except:
        driver = webdriver.Chrome()
    driver.get(uri['auth'])

    # Authorization
    email_elm = driver.find_element_by_name('email')
    password_elm = driver.find_element_by_name('pass')
    login_button = driver.find_element_by_id('install_allow')

    email_elm.send_keys(config['email'])
    password_elm.send_keys(config['password'])
    login_button.click()

    # Providing Access Permissions
    try:
        allow_button = driver.find_element_by_id('install_allow')
        allow_button.click()
    except:
        pass

    access_token = driver.current_url.split('access_token=')[1].split('&')[0]

    # Search target user info
    target_user = config['link'].split('/')[-1]
    driver.get(uri['user_get'] % target_user)
    target_user_info = json.loads(driver.find_element_by_tag_name('pre').text)['response'][0]

    # Receiving audio_list
    driver.get(uri['audio_list'] % (target_user_info['uid'], access_token))
    audio_list = json.loads(driver.find_element_by_tag_name('pre').text)
    driver.close()

    start_flag = raw_input('Total count audio - %s.\nStart downloading? (Y, n)\n' % len(audio_list['response'][1:]))

    if start_flag.lower() == 'n':
        return 0

    dir_name = target_user_info['first_name'] + '_' + target_user_info['last_name']
    path = os.path.join(config['path'], dir_name)
    if not os.path.exists(path):
        os.mkdir(path)

    fails_list = []
    success_count = 0

    # Downloading music
    for audio in audio_list['response'][1:]:
        name = (audio['artist'] + ' - ' + audio['title']).replace('/', '')
        file_name = os.path.join(path, name + '.mp3')
        if os.path.exists(file_name):
            continue
        print('Downloading: ' + file_name)
        try:
            r = requests.get(audio['url'], timeout=config['timeout'])
            with open(file_name, 'wb') as audio_file:
                audio_file.write(r.content)
            success_count += 1
        except:
            print('Fail!')
            fails_list.append(name)
            continue
    print('_____________')
    print('Total success: %s' % success_count)
    if fails_list:
        print 'Fails: %s' % len(fails_list)
        for name in fails_list:
            print(name)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='This script for download music from vk_friend list')
    parser.add_argument('--link', dest='link', required=True, help='Link to target user')
    parser.add_argument('--login', dest='login', required=True, help='Yours login')
    parser.add_argument('--pass', dest='password', required=True, help='Yours password')
    parser.add_argument('--path', dest='path', required=True, help='Path to directory')
    parser.add_argument('--timeout', type=float, dest='timeout', help='Timeout on response', default=5.0)
    parser.add_argument('--client_id', dest='client_id', help='Application id', default='5086323')
    args = parser.parse_args()

    conf = {
        'link': args.link,
        'path': args.path,
        'email': args.login,
        'password': args.password,
        'timeout': args.timeout,
        'client_id': args.client_id,
        'redirect_uri': 'https://oauth.vk.com/blank.html',
        'version_api': '5.37',
        'scope': 'audio',
    }

    uris = {
        'auth':
            'https://oauth.vk.com/authorize?client_id=%s&display=page&redirect_uri=%s&response_type=token&v=%s&scope=%s'
            % (conf['client_id'], conf['redirect_uri'], conf['version_api'], conf['scope']),
        'audio_list': 'https://api.vk.com/method/audio.get?owner_id=%s&access_token=%s',
        'user_get': 'https://api.vk.com/method/users.get?user_ids=%s',
    }

    get_audio(conf, uris)
